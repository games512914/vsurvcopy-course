extends CharacterBody2D

signal health_depleted
var health = 100.0
@onready var health_bar: ProgressBar = $HealthBar
@onready var camera: Camera2D = $Camera2D
var counter = 0.0

func _check_for_tiles():
	# TODO remove this, iterate nodes which are tile generators, if they aren't colliding with a tile (maybe use a layer specifically for tile collisions)
	#      then generate a tile in that generator
	if counter > 5:
		return 0
	var camera_viewport = camera.get_viewport_transform()
	for x in range(camera_viewport.origin.x, camera_viewport.origin.x + camera_viewport.x.x):
		for y in range(camera_viewport.origin.y, camera_viewport.origin.y + camera_viewport.y.y):
			print(x, y)

func _check_damage(delta):
	var overlapping_mobs: Array = %HurtBox.get_overlapping_bodies()
	health -= overlapping_mobs.size() * 10 * delta
	health_bar.value = health
	if health <= 0 and overlapping_mobs:
		health_depleted.emit()

func _physics_process(delta):
	var direction = Input.get_vector("move_left", "move_right", "move_up", "move_down")
	velocity = direction * 600
	var happy_boo = $HappyBoo
	happy_boo.animate_by_velocity(velocity)
	move_and_slide()
	_check_damage(delta)
	counter += delta
	_check_for_tiles()
	
