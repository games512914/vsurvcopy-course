extends TileMap

@export var noise_texture: NoiseTexture2D
@export var camera: Camera2D
var source_id = 0


func generate_chunk(x_offset: int, y_offset: int):
	var distribution = noise_texture.color_ramp
	var generated_image = noise_texture.get_image()
	for x in range(x_offset + generated_image.get_width()):
		for y in range(y_offset + generated_image.get_height()):
			var pixel = generated_image.get_pixel(x, y)
			set_cell(0, Vector2i(x, y), source_id, Vector2i(0, 0), distribution.colors.find(pixel))

func _ready():
	await noise_texture.changed
	generate_chunk(0, 0)

