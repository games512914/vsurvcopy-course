extends Node2D


func _play_idle_animation():
	%AnimationPlayer.play("idle")


func _play_walk_animation():
	%AnimationPlayer.play("walk")


func animate_by_velocity(velocity: Vector2):
	if velocity.length() > 0:
		_play_walk_animation()
	else:
		_play_idle_animation()
