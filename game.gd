extends Node2D


func _on_spawner_mob_spawned(mob_type, mob_position):
	var mob_instance = mob_type.instantiate()
	mob_instance.global_position = mob_position
	add_child(mob_instance)



func _on_player_health_depleted():
	%GameOver.visible = true

	get_tree().paused = true
