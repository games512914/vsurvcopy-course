extends CharacterBody2D

var health = 3

const DEATH_EFFECT = preload("res://smoke_explosion/smoke_explosion.tscn")
@onready var player = get_node("/root/Game/Player")
@onready var slime = $Slime

func _ready():
	slime.play_walk()

func _physics_process(delta):
	var direction = global_position.direction_to(player.global_position) 
	velocity = direction * 200
	move_and_slide()

func take_damage():
	health -= 1
	slime.play_hurt()
	if health == 0:
		var death_effect = DEATH_EFFECT.instantiate()
		queue_free()
		add_sibling(death_effect)
		death_effect.global_position = global_position
		
