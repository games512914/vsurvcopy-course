extends Area2D

const BULLET = preload("res://bullet.tscn")
@export var fire_rate = 1.0


func _physics_process(delta):
	var overlapping: Array[Node2D] = get_overlapping_bodies()
	if overlapping:
		look_at(overlapping[0].global_position)
	%FireRate.wait_time = fire_rate
	
func shoot():
	var new_bullet = BULLET.instantiate()
	new_bullet.global_position = %Cannon.global_position
	new_bullet.global_rotation = %Cannon.global_rotation
	%Cannon.add_child(new_bullet)


func _on_timer_timeout():
	shoot()
