extends Area2D

var ttl = 100

func _physics_process(delta):
	if ttl == 0:
		queue_free()
	var direction = Vector2.RIGHT.rotated(rotation)
	position += direction * 1000 * delta
	ttl -= 1


func _on_body_entered(body):
	queue_free()
	if body.has_method("take_damage"):
		body.take_damage()
