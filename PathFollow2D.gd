extends PathFollow2D

const SLIME = preload("res://mob.tscn")

signal mob_spawned(mob_type)

func _physics_process(delta):
	var spawn_location = randf()
	progress_ratio = spawn_location
	

func _on_timer_timeout():
	mob_spawned.emit(SLIME, global_position)
